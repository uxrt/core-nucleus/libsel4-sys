#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

extern crate rlibc;

#[allow(dead_code)]
#[cfg(any(target_arch = "x86", target_arch = "arm"))]
pub mod c_types {
    pub type c_uint = u32;
    pub type c_int = i32;

    pub type c_ulong = u32;
    pub type c_long = u32;

    pub type c_uchar = u8;
    pub type c_char = i8;
    pub type c_schar = i8;

    pub type c_ushort = u16;
    pub type c_short = i16;

    pub type c_ulonglong = u64;
    pub type c_longlong = i64;
}

#[cfg(any(target_arch = "x86", target_arch = "arm"))]
pub type seL4_RawWord = u32;

#[allow(dead_code)]
#[cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]
pub mod c_types {
    pub type c_uint = u32;
    pub type c_int = i32;

    pub type c_ulong = u64;
    pub type c_long = u64;

    pub type c_uchar = u8;
    pub type c_char = i8;
    pub type c_schar = i8;

    pub type c_ushort = u16;
    pub type c_short = i16;

    pub type c_ulonglong = u64;
    pub type c_longlong = i64;
}

#[cfg(any(target_arch = "x86_64", target_arch = "aarch64"))]
pub type seL4_RawWord = u64;

#[cfg(feature = "KernelPrinting")]
pub struct DebugOutHandle;


//TODO: allow tapping into this so that messages can be redirected to a user program (tapping into kernel messages should also be supported)
#[cfg(feature = "KernelPrinting")]
impl ::core::fmt::Write for DebugOutHandle {
    fn write_str(&mut self, s: &str) -> ::core::fmt::Result {
        for &b in s.as_bytes() {
            unsafe { self::seL4_DebugPutChar(b as i8) };
        }
        Ok(())
    }
}

#[allow(unused_variables)]
#[no_mangle]
pub unsafe extern "C" fn __assert_fail(
    mstr: *const c_types::c_char,
    file: *const c_types::c_char,
    line: c_types::c_int,
    function: *const c_types::c_char,
) {
    panic!("assertion failed");
}

#[no_mangle]
pub unsafe extern "C" fn stpcpy(
    dest: *mut c_types::c_schar,
    source: *const c_types::c_schar,
) -> *mut c_types::c_schar {
    for i in 0.. {
        *dest.offset(i) = *source.offset(i);
        if *dest.offset(i) == 0 {
            break;
        }
    }

    dest
}

#[no_mangle]
pub unsafe extern "C" fn strcpy(
    dest: *mut c_types::c_schar,
    source: *const c_types::c_schar,
) -> *mut c_types::c_schar {
    stpcpy(dest, source);
    dest
}


include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
