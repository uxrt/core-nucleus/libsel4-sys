# dummy-libsel4-sys

This is a hack to allow ```cargo check``` and the like to work on feL4-based
projects, which allows error/warning highlighting features of editors to work on
them. Since feL4 projects are built with a custom wrapper around cargo, many 
regular cargo commands are broken on them. This crate provides dummy versions of
all libsel4-sys functions and constants and doesn't require any custom build
tools, so it can be used with any Rust target. The libsel4-sys dependencies
should be specified similarly to this:

[target.'cfg(target_os = "sel4")'.dependencies]
libsel4-sys = { version = "0.5.0", path = "<path>/libsel4-sys" }
[target.'cfg(not(target_os = "sel4"))'.dependencies]
dummy-libsel4-sys = { version = "0.5.0", path = "<path>/libsel4-sys/dummy" }
